package de.sebse.fuplanner.tools.types;

public class News {
    public static final int CATEGORY_TRICKS = 0;
    public static final int CATEGORY_UPDATE = 1;

    private String title;
    private int category;
    private long date;
    private String text;

    public News(String title, int category, long date, String text) {
        this.title = title;
        this.category = category;
        this.date = date;
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public int getCategory() {
        return category;
    }

    public long getDate() {
        return date;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", category=" + category +
                ", date=" + date +
                ", text='" + text + '\'' +
                '}';
    }


}
