package de.sebse.fuplanner.tools.ui;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import de.sebse.fuplanner.R;

public class MealViewHolder extends ExpandableCardViewHolder {
    public final TextView mTitle;
    public final TextView mSubTitle;
    public final TextView mNotes;

    public final ImageView mIconVegan;
    public final ImageView mIconVegetarian;
    public final ImageView mIconBio;
    public final ImageView mIconMsc;


    public MealViewHolder(View view) {
        super(view);
        View outerView = getOuterView();
        View innerView = getInnerView();
        mTitle = outerView.findViewById(R.id.title);
        mSubTitle = outerView.findViewById(R.id.sub_title);
        mNotes = innerView.findViewById(R.id.notes);
        mIconVegan = innerView.findViewById(R.id.icon_vegan);
        mIconVegetarian = innerView.findViewById(R.id.icon_vegetarian);
        mIconBio = innerView.findViewById(R.id.icon_organic);
        mIconMsc = innerView.findViewById(R.id.icon_msc);
    }

    @NonNull
    @Override
    public String toString() {
        return super.toString() + " '" + mTitle.getText() + "' '" + mSubTitle.getText() + "'";
    }
}
