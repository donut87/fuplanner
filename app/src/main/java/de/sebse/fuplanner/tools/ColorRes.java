package de.sebse.fuplanner.tools;

import android.graphics.Paint;
import androidx.annotation.ColorInt;

public class ColorRes implements Color {
    @ColorInt private final int mResId;

    public ColorRes(@ColorInt int color) {
        this.mResId = color;
    }

    @Override
    public void setPaintColor(Paint paint) {
        paint.setColor(mResId);
    }
}
