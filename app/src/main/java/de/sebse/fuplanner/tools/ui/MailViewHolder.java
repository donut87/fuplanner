package de.sebse.fuplanner.tools.ui;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import de.sebse.fuplanner.R;

public class MailViewHolder extends StringViewHolder {
    public final TextView mTitle;
    public final TextView mSubLeft;
    public final ImageView mIcon;

    public MailViewHolder(View view) {
        super(view);
        mTitle = view.findViewById(R.id.title);
        mSubLeft = view.findViewById(R.id.sub_left);
        mIcon = view.findViewById(R.id.icon);
    }

    @NonNull
    @Override
    public String toString() {
        return super.toString() + " '" + mTitle.getText() + "' '" + mSubLeft.getText() + "'";
    }
}
