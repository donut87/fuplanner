package de.sebse.fuplanner.tools.ui.treeview;

import android.view.View;
import android.widget.TextView;

import de.sebse.fuplanner.R;
import de.sebse.fuplanner.services.kvv.types.Resource;

/**
 * Created by tlh on 2016/10/1 :)
 */

public class FileNodeBinder extends TreeViewBinder<FileNodeBinder.ViewHolder> {
    @Override
    public ViewHolder provideViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    public void bindView(ViewHolder holder, int position, TreeNode node) {
        Resource.File fileNode = (Resource.File) node.getContent();
        holder.tvName.setText(fileNode.getTitle());
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_file;
    }

    public class ViewHolder extends TreeViewBinder.ViewHolder {
        final TextView tvName;

        ViewHolder(View rootView) {
            super(rootView);
            this.tvName = rootView.findViewById(R.id.tv_name);
        }

    }
}
