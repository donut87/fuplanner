package de.sebse.fuplanner.tools.network;

import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import androidx.annotation.Nullable;

/**
 * Created by sebastian on 24.10.17.
 */
public class Result {
    @Nullable private final byte[] body;
    private final Map<String, String> headers;

    Result(@Nullable byte[] body, Map<String, String> headers) {
        this.body = body;
        this.headers = headers;
    }

    @Nullable
    public String getParsed() {
        if (this.body == null)
            return null;
        try {
            return new String(this.body, HttpHeaderParser.parseCharset(headers));
        } catch (UnsupportedEncodingException e) {
            return new String(this.body);
        }
    }

    @Nullable
    public byte[] getBytes() {
        return body;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }
}
