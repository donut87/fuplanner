package de.sebse.fuplanner.tools.ui;

import android.view.View;
import android.widget.TextView;

import de.sebse.fuplanner.R;

public class ShortcutViewHolder extends CustomViewHolder {
    public final TextView mLeft;
    public final TextView mRight;

    public ShortcutViewHolder(View view) {
        super(view);
        mLeft = view.findViewById(R.id.left);
        mRight = view.findViewById(R.id.right);
    }
}
