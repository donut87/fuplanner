package de.sebse.fuplanner.tools;

import android.content.Context;

import androidx.annotation.ArrayRes;
import androidx.annotation.StringRes;
import androidx.preference.PreferenceManager;

public class Preferences {
    public static String getStringArray(Context context, @ArrayRes int key) {
        String[] strings = context.getResources().getStringArray(key);
        return PreferenceManager.getDefaultSharedPreferences(context).getString(strings[0], strings[1]);
    }


    public static String getString(Context context, @StringRes int key) {
        String string = context.getResources().getString(key);
        return PreferenceManager.getDefaultSharedPreferences(context).getString(string, null);
    }
    public static void setString(Context context, @StringRes int key, String value) {
        String string = context.getResources().getString(key);
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(string, value).apply();
    }

    public static long getLong(Context context, @StringRes int key) {
        String string = context.getResources().getString(key);
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(string, 0);
    }
    public static void setLong(Context context, @StringRes int key, long value) {
        String string = context.getResources().getString(key);
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(string, value).apply();
    }

    public static boolean getBoolean(Context context, @StringRes int key) {
        String string = context.getResources().getString(key);
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(string, false);
    }
    public static void setBoolean(Context context, @StringRes int key, boolean value) {
        String string = context.getResources().getString(key);
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(string, value).apply();
    }
}
