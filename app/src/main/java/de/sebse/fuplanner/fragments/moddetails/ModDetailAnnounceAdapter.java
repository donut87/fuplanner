package de.sebse.fuplanner.fragments.moddetails;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cunoraz.tagview.Tag;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.services.kvv.ui.Download;
import de.sebse.fuplanner.services.kvv.types.Announcement;
import de.sebse.fuplanner.services.kvv.types.Modules;
import de.sebse.fuplanner.tools.Regex;
import de.sebse.fuplanner.tools.UtilsDate;
import de.sebse.fuplanner.tools.ui.AnnouncementViewHolder;
import de.sebse.fuplanner.tools.ui.CustomViewHolder;

class ModDetailAnnounceAdapter extends RecyclerView.Adapter<CustomViewHolder> {

    @Nullable private Modules.Module mModule = null;
    @NonNull private final Download.OnDownloadRequestInterface requestInterface;

    ModDetailAnnounceAdapter(@NonNull Download.OnDownloadRequestInterface requestInterface) {
        this.requestInterface = requestInterface;
    }

    public void setModule(Modules.Module module) {
        this.mModule = module;
        this.setModule();
    }

    private void setModule() {
        this.notifyDataSetChanged();
    }

    @NonNull
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.list_announcement_items, viewGroup, false);
            return new AnnouncementViewHolder(view);
        } else {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.list_all_no_items, viewGroup, false);
            return new CustomViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder customHolder, int position) {
        if (customHolder instanceof AnnouncementViewHolder) {
            AnnouncementViewHolder holder = (AnnouncementViewHolder) customHolder;
            holder.reset();
            Announcement item = getAnnouncement(position);
            holder.mTitle.setText(item.getTitle());
            holder.mSubTitle.setText(UtilsDate.getModifiedDateTime(holder.mView.getContext(), item.getCreatedOn()));
            holder.mTagGroup.removeAll();

            List<String> notes = item.getUrls();
            if (!notes.isEmpty()) {
                holder.mTagGroup.setVisibility(View.VISIBLE);
                for (int i = 0, notesSize = notes.size(); i < notesSize; i++) {
                    String name = urlToName(notes.get(i), i, holder.mView.getResources());
                    Tag tag = new Tag(name);
                    tag.id = i;
                    tag.layoutColor = ContextCompat.getColor(holder.mView.getContext(), R.color.colorFUBlue);
                    holder.mTagGroup.addTag(tag);
                }
                holder.mTagGroup.setOnTagClickListener((tag, i) -> {
                    String s = notes.get(i);
                    if (s != null) {
                        String name = urlToName(s, i, holder.mView.getResources());
                        requestInterface.request(name, s);
                    }
                });
            } else {
                holder.mTagGroup.setVisibility(View.GONE);
            }
            holder.mNotes.setText(item.getBody());
        }
    }

    private String urlToName(String url, int index, Resources res) {

        try {
            return URLDecoder.decode(Regex.regex("/([^/]*)$", url), "UTF-8");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return res.getString(R.string.attachment_nr, index);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return res.getString(R.string.attachment_nr, index);
        }
    }

    @Override
    public int getItemCount() {
        if (mModule != null && mModule.announcements != null)
            return Math.max(mModule.announcements.size(), 1);
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (mModule != null && mModule.announcements != null && mModule.announcements.size() == 0)
            return 1;
        else
            return 0;
    }

    private Announcement getAnnouncement(int index) {
        if (mModule != null && mModule.announcements != null)
            return mModule.announcements.get(index);
        else
            return null;
    }
}