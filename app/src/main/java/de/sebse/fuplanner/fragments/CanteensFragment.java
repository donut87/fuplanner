package de.sebse.fuplanner.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.sebse.fuplanner.MainActivity;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.services.canteen.CanteenBrowser;
import de.sebse.fuplanner.tools.MainActivityListener;
import de.sebse.fuplanner.tools.logging.Logger;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnCanteensFragmentInteractionListener}
 * interface.
 */
public class CanteensFragment extends Fragment {
    private OnCanteensFragmentInteractionListener mListener;
    private final Logger log = new Logger(this);
    private CanteensAdapter adapter;
    private SwipeRefreshLayout swipeLayout;
    private MainActivityListener mMainActivityListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CanteensFragment() {
    }

    public static CanteensFragment newInstance() {
        CanteensFragment fragment = new CanteensFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        // Set the adapter
        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new CanteensAdapter(mListener);
        recyclerView.setAdapter(adapter);

        // Getting SwipeContainerLayout
        swipeLayout = view.findViewById(R.id.swipe_container);
        // Adding Listener
        swipeLayout.setOnRefreshListener(() -> refresh(true));
        refresh(false);

        return view;
    }

    private void refresh(boolean forceRefresh) {
        if (getActivity() != null) {
            CanteenBrowser browser = ((MainActivity) getActivity()).getCanteenBrowser();
            browser.getCanteens(success -> {
                adapter.setCanteens(success);
                //if (mMainActivityListener != null)
                //    mMainActivityListener.refreshNavigation();
                swipeLayout.setRefreshing(false);
            }, error -> {
                log.e(error.toString());
                swipeLayout.setRefreshing(false);
            }, forceRefresh);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCanteensFragmentInteractionListener) {
            mListener = (OnCanteensFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCanteensFragmentInteractionListener");
        }
        if (context instanceof MainActivityListener) {
            mMainActivityListener = (MainActivityListener) context;
            mMainActivityListener.onTitleTextChange(R.string.canteens);
        } else
            throw new RuntimeException(context.toString() + " must implement MainActivityListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mMainActivityListener = null;
    }

    public interface OnCanteensFragmentInteractionListener {
        void onCanteensFragmentInteraction(int id);
    }
}
