package de.sebse.fuplanner.fragments.moddetails;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.tools.MainActivityListener;
import de.sebse.fuplanner.tools.logging.Logger;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ModDetailOverviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ModDetailOverviewFragment extends Fragment {
    private static final String ARG_POSITION = "itemPosition";

    private String mItemPos;
    private final Logger log = new Logger(this);
    private ModDetailOverviewAdapter adapter;
    private SwipeRefreshLayout swipeLayout;
    @Nullable private ModDetailListener mDetailListener;
    @Nullable private MainActivityListener mListener;


    public ModDetailOverviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param itemPosition Item position in module list.
     * @return A new instance of fragment ModDetailOverviewFragment.
     */
    public static ModDetailOverviewFragment newInstance(String itemPosition) {
        ModDetailOverviewFragment fragment = new ModDetailOverviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_POSITION, itemPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mItemPos = getArguments().getString(ARG_POSITION);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        // Set the adapter
        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new ModDetailOverviewAdapter(mDetailListener);
        recyclerView.setAdapter(adapter);

        // Getting SwipeContainerLayout
        swipeLayout = view.findViewById(R.id.swipe_container);
        // Adding Listener
        swipeLayout.setOnRefreshListener(() -> refresh(true));
        refresh(false);

        return view;
    }




    private void refresh(boolean forceRefresh) {
        if (mListener != null) {
            mListener.getKVV().modules().details().recv(mItemPos, pair -> {
                adapter.setModule(pair.first);
                if (pair.second)
                    swipeLayout.setRefreshing(false);
            }, error -> {
                swipeLayout.setRefreshing(false);
                log.e(error);
            }, forceRefresh);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivityListener) {
            this.mListener = ((MainActivityListener) context);
        } else
            throw new RuntimeException(context.toString() + " must implement MainActivityListener");


        super.onAttach(context);
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            if (parentFragment instanceof ModDetailListener) {
                mDetailListener = (ModDetailListener) parentFragment;
            } else {
                throw new RuntimeException(context.toString() + " must implement ModDetailListener");
            }
        } else log.w("No parent fragment!");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }
}
