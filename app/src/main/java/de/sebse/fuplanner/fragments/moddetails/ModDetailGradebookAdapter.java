package de.sebse.fuplanner.fragments.moddetails;

import android.annotation.SuppressLint;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.services.kvv.types.Grade;
import de.sebse.fuplanner.services.kvv.types.Modules;
import de.sebse.fuplanner.tools.ui.StringViewHolder;

class ModDetailGradebookAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_TOTAL = 0;
    private static final int TYPE_GRADE = 1;

    private static final int SECTION_GRADE = 0;

    private Modules.Module mValue;
    private final ArrayList<Pair<Integer, Integer>> mPositionalData;

    ModDetailGradebookAdapter() {
        mValue = null;
        mPositionalData = new ArrayList<>();
        //mListener = listener;
    }

    public void setModule(Modules.Module module) {
        mValue = module;
        this.setModule();
    }

    private void setModule() {
        mPositionalData.clear();
        mPositionalData.add(new Pair<>(TYPE_TOTAL, SECTION_GRADE));
        for (int i = 0; i < getGradesCount(); i++) {
            mPositionalData.add(new Pair<>(TYPE_GRADE, SECTION_GRADE +1024*i));
        }

        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_TOTAL:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_all_caption, parent, false);
                return new StringViewHolder(view);
            case TYPE_GRADE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_moddetails_gradebook, parent, false);
                return new GradeViewHolder(view);
            default:
                //noinspection ConstantConditions
                return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        // Note that unlike in ListView adapters, types don't have to be contiguous
        if (position < mPositionalData.size())
            return mPositionalData.get(position).first;
        else return -1;
    }

    @SuppressLint("StringFormatInvalid")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        if (mValue == null || position > mPositionalData.size())
            return;
        Pair<Integer, Integer> data = mPositionalData.get(position);
        switch (data.first) {
            case TYPE_TOTAL:
                StringViewHolder h = (StringViewHolder) holder;
                h.mString.setText(h.mView.getResources().getString(R.string.current_percentage, mValue.getGradebookPercent()*100));
            break;
            case TYPE_GRADE:
                int index = data.second / 1024;
                GradeViewHolder i = (GradeViewHolder) holder;
                Grade gradebook = mValue.gradebook.get(index);

                i.mTitle.setText(gradebook.getItemName());
                i.mGrade.setText(String.valueOf(gradebook.getPoints()));
                i.mGradeMax.setText(String.valueOf(gradebook.getMaxPoints()));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mPositionalData.size();
    }

    private int getGradesCount() {
        if (mValue.gradebook != null)
            return mValue.gradebook.size();
        return 0;
    }









    private class GradeViewHolder extends RecyclerView.ViewHolder {
        private final TextView mGrade;
        private final TextView mGradeMax;
        private final TextView mTitle;

        GradeViewHolder(View view) {
            super(view);
            mTitle = view.findViewById(R.id.title);
            mGrade = view.findViewById(R.id.grade);
            mGradeMax = view.findViewById(R.id.grade_max);
        }

        @NonNull
        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + " '" + mGrade.getText() + " '" + mGradeMax.getText() + "'";
        }
    }
}
