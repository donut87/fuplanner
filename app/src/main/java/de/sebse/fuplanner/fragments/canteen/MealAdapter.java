package de.sebse.fuplanner.fragments.canteen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.recyclerview.widget.RecyclerView;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.services.canteen.types.Day;
import de.sebse.fuplanner.services.canteen.types.Meal;
import de.sebse.fuplanner.tools.Preferences;
import de.sebse.fuplanner.tools.ui.MealViewHolder;
import de.sebse.fuplanner.tools.ui.StringViewHolder;

class MealAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final String[] CATEGORY_KEYS = new String[]{"Essen", "Aktionen", "Beilagen", "Desserts", "Salate", "Suppen", "Vorspeisen"};
    @StringRes
    private final int[] CATEGORY_VALS = new int[]{R.string.meals, R.string.special_meals, R.string.side_dishes, R.string.desserts, R.string.salads, R.string.soups, R.string.starters};
    @StringRes
    private final int CATEGORY_OTHER = R.string.others;
    private final ArrayList<Object> matches = new ArrayList<>();

    private Day mDay = null;
    private final Context mContext;

    public MealAdapter(Context context) {
        mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.list_canteen_items, viewGroup, false);
            return new MealViewHolder(view);
        } else {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.list_all_caption, viewGroup, false);
            return new StringViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == 0) {
            MealViewHolder viewHolder = ((MealViewHolder) holder);
            viewHolder.reset();
            Meal meal = getMeal(position);
            viewHolder.mTitle.setText(meal.getName());
            String value;
            switch (Preferences.getStringArray(mContext, R.array.pref_price_group)) {
                case "student":
                    if (meal.getPriceStdnt() < 0)
                        value = mContext.getResources().getString(R.string.no_price_available);
                    else
                        value = mContext.getString(R.string.price, meal.getPriceStdnt());
                    break;
                case "employee":
                    if (meal.getPriceEmply() < 0)
                        value = mContext.getResources().getString(R.string.no_price_available);
                    else
                        value = mContext.getString(R.string.price, meal.getPriceEmply());
                    break;
                case "other":
                    if (meal.getPriceOther() < 0)
                        value = mContext.getResources().getString(R.string.no_price_available);
                    else
                        value = mContext.getString(R.string.price, meal.getPriceOther());
                    break;
                default:
                    String value1;
                    if (meal.getPriceStdnt() < 0)
                        value1 = " -/- ";
                    else
                        value1 = mContext.getString(R.string.price, meal.getPriceStdnt());
                    String value2;
                    if (meal.getPriceEmply() < 0)
                        value2 = " -/- ";
                    else
                        value2 = mContext.getString(R.string.price, meal.getPriceEmply());
                    String value3;
                    if (meal.getPriceOther() < 0)
                        value3 = " -/- ";
                    else
                        value3 = mContext.getString(R.string.price, meal.getPriceOther());
                        value = mContext.getString(R.string.prices, value1, value2, value3);
            }
            viewHolder.mSubTitle.setText(value);
            StringBuilder string = new StringBuilder();
            List<String> notes = meal.getNotes();
            for (int i1 = 0, notesSize = notes.size(); i1 < notesSize; i1++) {
                if (i1 != 0)
                    string.append("\n");
                String s = notes.get(i1);
                string.append(" - ").append(s);
            }
            viewHolder.mNotes.setText(string.toString());
            viewHolder.mIconVegan.setVisibility(meal.getVegan() == Meal.VEGAN_VEGAN ? View.VISIBLE : View.GONE);
            viewHolder.mIconVegetarian.setVisibility(meal.getVegan() == Meal.VEGAN_VEGETARIAN ? View.VISIBLE : View.GONE);
            viewHolder.mIconBio.setVisibility((meal.getCertificates() & Meal.CERT_BIO) != 0 ? View.VISIBLE : View.GONE);
            viewHolder.mIconMsc.setVisibility((meal.getCertificates() & Meal.CERT_MSC) != 0 ? View.VISIBLE : View.GONE);
        } else {
            StringViewHolder viewHolder = ((StringViewHolder) holder);
            viewHolder.mString.setText(getHeading(position));
        }
    }

    @Override
    public int getItemCount() {
        return matches.size();
    }

    @Override
    public int getItemViewType(int position) {
        return matches.get(position) instanceof String ? 1 : 0;
    }

    private Meal getMeal(int position) {
        if (this.mDay != null)
            return this.mDay.get((Integer) matches.get(position));
        else
            return null;
    }

    private String getHeading(int position) {
        return (String) matches.get(position);
    }

    public void setDay(Day day) {
        this.mDay = day;
        this.setDay();
    }

    public void setDay() {
        HashMap<String, ArrayList<Integer>> map = new HashMap<>();
        for (int i = 0; i < this.mDay.size(); i++) {
            String category = this.mDay.get(i).getCategory();
            boolean found = false;
            for (String category_key : CATEGORY_KEYS) {
                if (category_key.equals(category)) {
                    found = true;
                    break;
                }
            }
            if (!found) category = "---";
            ArrayList<Integer> list = map.get(category);
            if (list == null) {
                list = new ArrayList<>();
                map.put(category, list);
            }
            list.add(i);
        }
        matches.clear();
        for (int i = 0; i < CATEGORY_KEYS.length; i++) {
            ArrayList<Integer> list = map.get(CATEGORY_KEYS[i]);
            if (list != null) {
                matches.add(mContext.getString(CATEGORY_VALS[i]));
                matches.addAll(list);
            }
        }
        ArrayList<Integer> list = map.get("---");
        if (list != null) {
            matches.add(mContext.getString(CATEGORY_OTHER));
            matches.addAll(list);
        }
        this.notifyDataSetChanged();
    }
}