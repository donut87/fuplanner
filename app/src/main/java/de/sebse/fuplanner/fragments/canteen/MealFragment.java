package de.sebse.fuplanner.fragments.canteen;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.sebse.fuplanner.MainActivity;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.services.canteen.CanteenBrowser;
import de.sebse.fuplanner.services.canteen.types.Canteen;
import de.sebse.fuplanner.services.canteen.types.Day;
import de.sebse.fuplanner.tools.logging.Logger;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MealFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MealFragment extends Fragment {
    private static final String ARG_POSITION = "canteenId";
    private static final String ARG_DAY_ID = "dayPosition";

    private int mCanteenId;
    private int mDayPosition;
    private final Logger log = new Logger(this);
    private MealAdapter adapter;
    private SwipeRefreshLayout swipeLayout;
    private DaySwitcherListener mListener;


    public MealFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param canteenId ID od current canteen.
     * @param dayPosition day to show.
     * @return A new instance of fragment MealFragment.
     */
    public static MealFragment newInstance(int canteenId, int dayPosition) {
        MealFragment fragment = new MealFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, canteenId);
        args.putInt(ARG_DAY_ID, dayPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCanteenId = getArguments().getInt(ARG_POSITION);
            mDayPosition = getArguments().getInt(ARG_DAY_ID);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        // Set the adapter
        RecyclerView expandableListView = view.findViewById(R.id.list);
        adapter = new MealAdapter(getContext());
        expandableListView.setAdapter(adapter);

        // Getting SwipeContainerLayout
        swipeLayout = view.findViewById(R.id.swipe_container);
        // Adding Listener
        swipeLayout.setOnRefreshListener(() -> mListener.onChildRefresh(callback -> refresh(true), error -> refresh(true)));
        refresh(false);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            if (parentFragment instanceof DaySwitcherListener) {
                mListener = (DaySwitcherListener) parentFragment;
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement DaySwitcherListener");
            }
        } else log.w("No parent fragment!");
    }

    private void refresh(boolean forceRefresh) {
        if (getActivity() != null) {
            CanteenBrowser browser = ((MainActivity) getActivity()).getCanteenBrowser();
            browser.getCanteens(canteens -> {
                Canteen canteen = canteens.getCanteen(mCanteenId);
                browser.getCanteen(canteen, success -> {
                    Day day = success.get(mDayPosition);
                    adapter.setDay(day);
                    browser.getDay(day, success1 -> {
                        adapter.setDay();
                        swipeLayout.setRefreshing(false);
                    }, error -> {
                        swipeLayout.setRefreshing(false);
                        log.e(error);
                    }, forceRefresh);
                }, error -> {
                    swipeLayout.setRefreshing(false);
                    log.e(error);
                }, forceRefresh);
            }, error -> {
                swipeLayout.setRefreshing(false);
                log.e(error);
            }, forceRefresh);
        }
    }
}
