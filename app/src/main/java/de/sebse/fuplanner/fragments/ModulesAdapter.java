package de.sebse.fuplanner.fragments;

import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.fragments.ModulesFragment.OnModulesFragmentInteractionListener;
import de.sebse.fuplanner.services.kvv.types.Lecturer;
import de.sebse.fuplanner.services.kvv.types.Modules;
import de.sebse.fuplanner.services.kvv.types.Semester;
import de.sebse.fuplanner.tools.ui.CustomViewHolder;
import de.sebse.fuplanner.tools.ui.ItemViewHolder;
import de.sebse.fuplanner.tools.ui.StringViewHolder;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Modules.Module} and makes a call to the
 * specified {@link OnModulesFragmentInteractionListener}.
 */
class ModulesAdapter extends RecyclerView.Adapter<CustomViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 2;

    private Modules mValues;
    private final OnModulesFragmentInteractionListener mListener;
    private final ArrayList<Pair<Integer, Object>> mPositionalData;

    ModulesAdapter(OnModulesFragmentInteractionListener listener) {
        mValues = null;
        mListener = listener;
        mPositionalData = new ArrayList<>();
    }

    public void setModules(Modules modules) {
        mValues = modules;
        mPositionalData.clear();
        Semester lastSemester = new Semester(Semester.SEM_WS, 0);
        for (Modules.Module module : mValues) {
            Semester semester = module.semester;
            if (semester != null && !semester.equals(lastSemester) || semester == null && lastSemester != null) {
                mPositionalData.add(new Pair<>(TYPE_HEADER, semester));
                lastSemester = semester;
            }
            mPositionalData.add(new Pair<>(TYPE_ITEM, module));
        }
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return mPositionalData.get(position).first;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_all_caption, parent, false);
            return new StringViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_all_items, parent, false);
            return new ItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        Pair<Integer, Object> pair = mPositionalData.get(holder.getAdapterPosition());
        if (pair.first == TYPE_HEADER) {
            StringViewHolder sHolder = (StringViewHolder) holder;
            String localizedSemester;
            Semester semester = (Semester) pair.second;
            if (semester == null)
                localizedSemester = holder.mView.getResources().getString(R.string.others);
            else if (semester.getType() == Semester.SEM_WS)
                localizedSemester = holder.mView.getResources().getString(R.string.winter_semester, semester.getYear(), semester.getYear()+1);
            else
                localizedSemester = holder.mView.getResources().getString(R.string.summer_semester, semester.getYear());
            sHolder.mString.setText(localizedSemester);
        } else if (pair.first == TYPE_ITEM) {
            ItemViewHolder iHolder = (ItemViewHolder) holder;
            Modules.Module module = ((Modules.Module) pair.second);
            iHolder.mTitle.setText(module.title);
            StringBuilder lecturers = new StringBuilder();
            for (Lecturer lecturer: module.lecturer) {
                if (!lecturer.isResponsible())
                    continue;
                if (lecturers.length() > 0)
                    lecturers.append(", ");
                lecturers.append(lecturer.getNameShort());
            }
            iHolder.mSubLeft.setText(lecturers);
            iHolder.mSubRight.setText(module.type);

            iHolder.mView.setOnClickListener(v -> {
                if (mListener != null) {
                    mListener.onModulesFragmentInteraction(module.getID());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mPositionalData.size();
    }
}
