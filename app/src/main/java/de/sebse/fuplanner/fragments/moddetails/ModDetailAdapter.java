package de.sebse.fuplanner.fragments.moddetails;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import de.sebse.fuplanner.R;

class ModDetailAdapter extends FragmentStatePagerAdapter {
    private final String mItemPos;
    private final Context mContext;

    ModDetailAdapter(FragmentManager fm, String itemPosition, Context context) {
        super(fm);
        this.mContext = context;
        this.mItemPos = itemPosition;
    }


    // Returns total number of pages
    @Override
    public int getCount() {
        return ModulePart.getPageCount();
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (ModulePart.getPartByPage(position)) {
            case ModulePart.OVERVIEW:
                return ModDetailOverviewFragment.newInstance(mItemPos);
            case ModulePart.ANNOUNCEMENT:
                return ModDetailAnnounceFragment.newInstance(mItemPos);
            case ModulePart.ASSIGNMENT:
                return ModDetailAssignmentFragment.newInstance(mItemPos);
            case ModulePart.EVENT:
                return ModDetailEventFragment.newInstance(mItemPos);
            case ModulePart.GRADEBOOK:
                return ModDetailGradebookFragment.newInstance(mItemPos);
            case ModulePart.RESOURCES:
                return ModDetailResourceFragment.newInstance(mItemPos);
            default:
                return null;
        }
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        switch (ModulePart.getPartByPage(position)) {
            case ModulePart.OVERVIEW:
                return this.mContext.getResources().getString(R.string.overview);
            case ModulePart.ANNOUNCEMENT:
                return this.mContext.getResources().getString(R.string.announcements);
            case ModulePart.ASSIGNMENT:
                return this.mContext.getResources().getString(R.string.assignments);
            case ModulePart.EVENT:
                return this.mContext.getResources().getString(R.string.events);
            case ModulePart.GRADEBOOK:
                return this.mContext.getResources().getString(R.string.gradebook);
            case ModulePart.RESOURCES:
                return this.mContext.getResources().getString(R.string.resources);
            default:
                return "";
        }
    }

}
