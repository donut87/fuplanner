package de.sebse.fuplanner.services.kvv.types;

import com.google.android.gms.common.internal.Objects;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import androidx.annotation.NonNull;
import de.sebse.fuplanner.tools.ColorRGB;

public class Event implements Serializable {
    private final String siteId;
    private final String id;
    private final String type;
    private final String title;
    private final long duration;
    private final long firstTime;
    private final String location;

    public Event(String id, String type, String title, long duration, long firstTime, String siteId, String location) {
        this.siteId = siteId;
        this.id = id;
        this.type = type;
        this.title = title;
        this.duration = duration;
        this.firstTime = firstTime;
        this.location = location
                .replace(" Übungsraum", "")
                .replace(" Konferenzraum", "")
                .replace(" Seminarraum", "");
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getType() {
        return this.type;
    }

    public long getStartDate() {
        return this.firstTime;
    }

    public long getEndDate() {
        return this.firstTime+this.duration;
    }

    public String getLocation() {
        return location;
    }

    public String getModuleId() {
        return siteId;
    }

    public ColorRGB getColor() {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
        byte[] encodedHash;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            encodedHash = digest.digest(siteId.getBytes(StandardCharsets.UTF_8));
        } else {
            encodedHash = digest.digest(siteId.getBytes());
        }
        int h = (0xff & encodedHash[0]) + (0xff & encodedHash[1]) * 255;
        h = h * 360 / 0xffff;
        //int s = 0xff & encodedHash[2];
        //s = s * 100 / 0xffff;
        //int v = 0xff & encodedHash[3];
        //v = v * 100 / 0xffff;

        // range for more beautiful colors
        h = h / 30 * 30;
        int s = 100;
        int v = 80;

        return hsvToRgb(h/360.0, s/100.0, v/100.0);
    }

    private static ColorRGB hsvToRgb(double hue, double saturation, double value) {
        int h = (int)(hue * 6);
        double f = hue * 6 - h;
        double p = value * (1 - saturation);
        double q = value * (1 - f * saturation);
        double t = value * (1 - (1 - f) * saturation);

        switch (h) {
            case 0: return new ColorRGB(value, t, p);
            case 1: return new ColorRGB(q, value, p);
            case 2: return new ColorRGB(p, value, t);
            case 3: return new ColorRGB(p, q, value);
            case 4: return new ColorRGB(t, p, value);
            case 5: return new ColorRGB(value, p, q);
            default: throw new RuntimeException("Something went wrong when converting from HSV to RGB. Input was " + hue + ", " + saturation + ", " + value);
        }
    }

    @NonNull
    @Override
    public String toString() {
        return "ID: "+getId()+
                "\nType: "+getType()+
                "\nStart Date: "+getStartDate()+
                "\nEnd date: "+getEndDate();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getType(), getStartDate(), getEndDate(), getTitle(), getLocation());
    }
}
