package de.sebse.fuplanner.services.kvv.types;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class LecturerStorage implements Serializable {
    private transient static final long RESAVE_TIMER = 1000L * 60 * 60 * 24 * 30;
    private static final String FILE_NAME = "LecturerStorageSaving";
    private static final String FILE_NAME_TIMESTAMP = "LecturerStorageSavingTimestamp";
    private transient long mLastTimestamp = 0;

    private HashMap<String, ArrayList<String>> mLecturersPerCourse = new HashMap<>();
    private HashMap<String, Long> mLecturersPerCourseRefresh = new HashMap<>();
    private HashMap<String, Lecturer> mLecturers = new HashMap<>();
    private HashMap<String, Long> mLecturersRefresh = new HashMap<>();

    public static LecturerStorage load(Context context) throws IOException, ClassNotFoundException {
        FileInputStream fis = context.openFileInput(FILE_NAME);
        ObjectInputStream is = new ObjectInputStream(fis);
        Object readObject = is.readObject();
        if (!(readObject instanceof LecturerStorage))
            return null;
        LecturerStorage storage = (LecturerStorage) readObject;
        is.close();
        fis.close();

        fis = context.openFileInput(FILE_NAME_TIMESTAMP);
        is = new ObjectInputStream(fis);
        storage.mLastTimestamp = is.readLong();
        is.close();
        fis.close();

        return storage;
    }

    public boolean isNewerVersionInStorage(Context context) throws IOException {
        FileInputStream fis = context.openFileInput(FILE_NAME_TIMESTAMP);
        ObjectInputStream is = new ObjectInputStream(fis);
        boolean result = this.mLastTimestamp < is.readLong();
        is.close();
        fis.close();
        return result;
    }

    public void save(Context context) throws IOException {
        FileOutputStream fos = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(this);
        os.close();
        fos.close();

        fos = context.openFileOutput(FILE_NAME_TIMESTAMP, Context.MODE_PRIVATE);
        os = new ObjectOutputStream(fos);
        this.mLastTimestamp = System.currentTimeMillis();
        os.writeLong(this.mLastTimestamp);
        os.close();
        fos.close();
    }

    public void setLecturersPerCourse(String courseID, ArrayList<String> lecturers) {
        mLecturersPerCourse.put(courseID, lecturers);
        mLecturersPerCourseRefresh.put(courseID, System.currentTimeMillis());
    }

    public void setLecturer(String lecturerID, Lecturer lecturer) {
        mLecturers.put(lecturerID, lecturer);
        mLecturersRefresh.put(lecturerID, System.currentTimeMillis());
    }

    public ArrayList<String> getLecturersPerCourse(String courseID) {
        if (!mLecturersPerCourseRefresh.containsKey(courseID))
            return null;
        //noinspection ConstantConditions
        if (mLecturersPerCourseRefresh.get(courseID) + RESAVE_TIMER < System.currentTimeMillis())
            return null;
        return mLecturersPerCourse.get(courseID);
    }

    public Lecturer getLecturer(String lecturerID) {
        if (!mLecturersRefresh.containsKey(lecturerID))
            return null;
        //noinspection ConstantConditions
        if (mLecturersRefresh.get(lecturerID) + RESAVE_TIMER < System.currentTimeMillis())
            return null;
        return mLecturers.get(lecturerID);
    }
}
