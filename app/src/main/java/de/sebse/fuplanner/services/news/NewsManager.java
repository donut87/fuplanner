package de.sebse.fuplanner.services.news;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import de.sebse.fuplanner.R;
import de.sebse.fuplanner.tools.UtilsDate;
import de.sebse.fuplanner.tools.network.NetworkCallback;
import de.sebse.fuplanner.tools.network.NetworkError;
import de.sebse.fuplanner.tools.network.NetworkErrorCallback;
import de.sebse.fuplanner.tools.types.NewsList;

public class NewsManager {
    private NewsList mNewsList;
    private Context mContext;

    public NewsManager(Context context) {
        this.mContext = context;
    }

    public void recv(final NetworkCallback<NewsList> callback, final NetworkErrorCallback errorCallback) {
        recv(callback, errorCallback, false);
    }

    public void recv(final NetworkCallback<NewsList> callback, final NetworkErrorCallback errorCallback, final boolean forceRefresh) {
        if (!forceRefresh && mNewsList != null) {
            callback.onResponse(mNewsList);
            return;
        }
        String fromAsset = loadJSONFromAsset();
        if (fromAsset == null)
            return;
        String language = Locale.getDefault().getLanguage();
        JSONArray news;
        NewsList dates = new NewsList();
        try {
            JSONObject json = new JSONObject(fromAsset);
            news = json.getJSONArray("news");
        } catch (JSONException e) {
            e.printStackTrace();
            errorCallback.onError(new NetworkError(300100, 500, "Parsing news list failed!"));
            return;
        }

        for (int i = news.length() - 1; i >= 0; i--) {
            try {
                String title = news.getJSONObject(i).optString("title_" + language, null);
                if (title == null)
                    title = news.getJSONObject(i).getString("title");
                String categoryString = news.getJSONObject(i).getString("category");
                int category;
                if (categoryString.equals("CATEGORY_TRICKS"))
                    category = de.sebse.fuplanner.tools.types.News.CATEGORY_TRICKS;
                else
                    category = de.sebse.fuplanner.tools.types.News.CATEGORY_UPDATE;
                String dateString = news.getJSONObject(i).getString("date");
                long date = UtilsDate.stringToMillis(dateString, "dd.MM.yyyy");
                String text = news.getJSONObject(i).optString("text_" + language, null);
                if (text == null)
                    text = news.getJSONObject(i).getString("text");
                de.sebse.fuplanner.tools.types.News event = new de.sebse.fuplanner.tools.types.News(title, category, date, text);
                dates.add(event);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mNewsList = dates;
        callback.onResponse(mNewsList);
    }

    private String loadJSONFromAsset() {
        try {
            InputStream is = mContext.getResources().openRawResource(R.raw.news);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
