package de.sebse.fuplanner.services.canteen.types;

import java.util.Calendar;

import de.sebse.fuplanner.tools.SortedList;

public class SortedListDay extends SortedList<Day, Calendar, String> {
    @Override
    public int compare(Day o1, Day o2) {
        return Canteen.calendarToKey(o1.getCalendar()).compareTo(Canteen.calendarToKey(o2.getCalendar()));
    }

    @Override
    public boolean hasIdentifier(Day o1, Calendar id) {
        return Canteen.calendarToKey(o1.getCalendar()).equals(Canteen.calendarToKey(id));
    }

    @Override
    public boolean hasFilter(Day o1, String filter) {
        return false;
    }
}
