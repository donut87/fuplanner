package de.sebse.fuplanner.services.kvv.types;

import java.io.Serializable;

import androidx.annotation.Nullable;
import de.sebse.fuplanner.tools.Regex;

public class Semester implements Serializable {
    public static final int SEM_WS = 1;
    public static final int SEM_SS = 2;
    private int type;
    private int year;

    public Semester(int type, int year) {
        this.type = type;
        this.year = year;
    }

    public Semester(String semester_string) throws NoSuchFieldException {
        /*Semester sem = null;
            if (semester != null) {
                sem = new Semester(semester)
                semester = semester.replace("SS", "S");
                semester = semester.replaceAll("[0-9]{2}([0-9]{2})", "$1");
            }*/


        /*String s1type = Regex.regex("^(S|WS) ", a);
        int s1year = Integer.parseInt(Regex.regex("^(S|WS) ([0-9]{2})", a, 2));
        String s2type = Regex.regex("^(S|WS) ", b);
        int s2year = Integer.parseInt(Regex.regex("^(S|WS) ([0-9]{2})", b, 2));*/

        String type = Regex.regex("^(SS|WS) ", semester_string);
        String year = Regex.regex("^(SS|WS) ([0-9]{2})", semester_string, 2);
        this.type = type.equals("SS") ? SEM_SS : SEM_WS;
        this.year = Integer.parseInt(year);
    }

    public int getType() {
        return type;
    }

    public int getYear() {
        return year;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof Semester) {
            Semester other = (Semester) obj;
            return other.type == type && other.year == year;
        }
        return false;
    }
}
