package de.sebse.fuplanner.services.kvv;

import com.android.volley.NetworkResponse;

import de.sebse.fuplanner.services.kvv.types.LoginTokenKVV;
import de.sebse.fuplanner.tools.CustomAccountManager;

public interface KVVListener {
    default void onLogin(LoginTokenKVV token, boolean isOnlyRefresh) {}

    default void onLogout() {}

    default void onModuleListChange() {}

    default void onKVVNetworkResponse(NetworkResponse error) {}

    CustomAccountManager getAccountManager();
}
