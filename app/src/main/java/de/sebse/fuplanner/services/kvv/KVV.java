package de.sebse.fuplanner.services.kvv;

import android.content.Context;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class KVV {
    private final HashMap<String, Object> addons = new HashMap<>();
    private final KVVListener mListener;
    private final Context mContext;

    public KVV(KVVListener listener, Context context) {
        this.mListener = listener;
        this.mContext = context;
    }

    @NotNull
    public Login account() {
        return (Login) addAndGet("account", () -> new Login(mListener, mContext));
    }

    @NotNull
    public Modules modules() {
        return (Modules) addAndGet("module", () -> new Modules(account(), mListener, mContext));
    }






    @NotNull
    private Object addAndGet(@NotNull String addon, @NotNull ModuleCreatorInterface creatorInterface) {
        Object o = addons.get(addon);
        if (o == null) {
            o = creatorInterface.create();
            addons.put(addon, o);
        }
        return o;
    }

    private interface ModuleCreatorInterface {
        @NotNull Object create();
    }
}
