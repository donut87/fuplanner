package de.sebse.fuplanner.services.kvv.types;

import de.sebse.fuplanner.tools.DateSortedList;

public class AssignmentList extends DateSortedList<Assignment> {

    @Override
    public long getDateByItem(Assignment item) {
        return item.getDueDate();
    }

    @Override
    public boolean reversed() {
        return true;
    }
}
