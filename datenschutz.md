# Datenschutzrichtlinie

Im Folgenden klären wir über die Datenverarbeitung der Daten innerhalb der Android-App FUPlanner (im Folgenden "App" genannt) nach der Rechtsgrundlage von [Art. 6 der DSGVO](https://dejure.org/gesetze/DSGVO/6.html) auf. Wir informieren wie die Daten innerhalb der App gespeichert werden und welche Informationen mit den Servern des [KVV](https://kvv.imp.fu-berlin.de/), der [OpenMensa](https://openmensa.org/) und [Googles Smart Lock](https://get.google.com/smartlock/) (optional, siehe unten) ausgetauscht werden.

## Rechte des Nutzers

Im Folgenden informieren wir über die Rechte des Nutzers.

### Widerspruchsrecht

Nach [Art. 21 der DSGVO](https://dejure.org/gesetze/DSGVO/21.html) hat der Nutzer das Recht Widerspruch auf die Erhebung, Speicherung und Verarbeitung der Daten einzulegen. Da die App nur Daten von Drittservern abfragt innerhalb der App abspeichert und keine eigenen Server betrieben werden, muss der Widerspruch auf den jewiligen Diensten erfolgen. Die Speicherung der Daten innerhalb der App kann durch eine Deinstallation der App unterbunden werden.

### Recht auf Auskunft

Sämliche vom Benutzer innerhalb der App gespeicherten Daten (siehe unten) sind innerhalb der App jederzeit einsehbar. Der Nutzer hat das Recht sich die Auskunft jederzeit selbstständig zu anzueignen.

### Recht auf Berichtigung und Löschung

Der Nutzer hat das Recht die Daten jederzeit durch eine Deinstallation vollständig zu enfernen. Die Berichtigung der gespeicherten Daten erfolgt durch eine Korrektur in der zentralen Nutzerverwaltung beim ZEDAT der FU Berlin. Eine Änderung der Daten aus dem KVV (wie Wahl der Module, etc.) muss, sofern sie nicht direkt in der App getätigt werden kann, über das KVV der FU Berlin erfolgen.

### Beschwerderecht bei einer Aufsichtsbehörde

Nach [Art. 77 der DSGVO](https://dejure.org/gesetze/DSGVO/77.html) hat der Nutzer das Recht sich bei der zuständigen Aufsichtsbehörde zu beschweren.

### Recht auf Datenübertragbarkeit

Da die App keine eigenen Daten erhebt, können diese nach [Art. 20 der DSGVO](https://dejure.org/gesetze/DSGVO/20.html) nicht von der App bereit gestellt werden. Einstellungen und Optionen, die der Benutzer zur Konfiguration festlegt, können innerhalb der App vollständig eingesehen werden.

Um die Daten der jeweiligen verbundenen Dienste, insbesondere von Google Smart Lock und KVV einzusehen, muss der Nutzer sich technisch begründet an den jeweiligen Dienst direkt wenden.

## Allgemeines zur Datenverarbeitung

### Umfang der personenbezogenen Datenverarbeitung

Die Verarbeitung der Daten des Nutzers erfolgt nur im Zweck der Visualisierung und Zusammenstellung der Daten der angebundenen Dienste. Eine Verarbeitung der Daten erfolgt nur nach einwilligung des Nutzers mit der Ausnahme in technisch unmöglichen Fällen, in denen die Verarbeitung der Daten durch gesetzliche Voschreiften gestattet ist.

### Speicherdauer

Die Daten werden nur für die Dauer der Benutzung der App innerhalb der App gespeichert. Nach einer Deinstallation werden keine Daten mehr vorgehalten. Zur Löschung der Originaldaten der verwendeten Dienste muss von den jeweiligen Diensten getätigt werden.

## Verwendete und gespeicherte Daten

Im Folgenden sind die gespeicherten Daten der App und die Verwendung von den externen Diensten genauer erläutert.

### Google Smart Lock

Um ein Login-Vorgang zu beschleunigen wird Google Smart Lock verwendet. Dies ist ein Anmeldeinformations-Manager von google, der unter anderem auch eine sichere Speicherung von Passwörter ermöglicht. Bei einem erfolgreichen Login wird dem Nutzer über die Google-Play-Services sie Möglichkeit geboten das Passwort über Google Smart Lock zu speichern. Nach freiwilliger Einwilligung wird der Benutzername und das Passwort verschlüsselt an Google übergeben und verschlüsselt gespeichert. Google ist theoretisch in der Lage das Passwort für kriminelle Zwecke entschlüsselt der Polizei zu übergeben. Außerdem wird während der Übertragung die IP-Adresse und der verwendete Google-Account mit den Google-Servern ausgetauscht.

Bei einer fehlenden Einwilligung des Benutzers Google Smart Lock zu benutzen, muss bei jedem erneuten Einloggen (Ablauf der Sitzung oder erstmaliger Login mit einem Account) der Benutzername und Passwort wiederholt eingegeben werden.

#### Rechtsgrundlage für die Datenverarbeitung

Die Rechtsgrundlage für die Verwendung personenbezogener Daten unter der Verwendung von Google Smart Lock ist nach einer vorliegenden Einwilligung des Nutzers hierfür der [Art. 6 der DSGVO](https://dejure.org/gesetze/DSGVO/6.html).

#### Widerrufsrecht

Um eine Speicherung des Passworts zu wiederrufen, kann der Nutzer den [Hilfeseiten zu Smart Lock](https://support.google.com/accounts/answer/6197437?co=GENIE.Platform%3DDesktop&oco=1) folgen. 

#### Dauer der Speicherung und Beseitigungsmöglichkeit

Bis zum Widerruf der Daten bei Google oder zum Logout innerhalb der App bleiben die Anmeldedaten auf den Servern von Google gespeichert. Die App selbst speichert zur Sicherheit der Nutzer keine Nutzernamen oder Passwörter. Eine Deinstallation ohne zuvorigen Logout ist nicht ausreichend, da es technisch nicht umsetzbar ist.

### KVV der FU Berlin

Bei einem Login loggt sich die App auf dem KVV der FU Berlin ein. Dabei wird neben den Anmeldedaten die IP-Adresse des Nutzers mitgeschickt. Die tempräre Speicherung der IP-Adress durch das System wird benötigt, um die Auslieferung der Netzwerkanfrage zu ermöglichen. 

Als Antwort beim Loginvorgang werden Cookies gespeichert, die zur erneuten Identifizierung bei weiteren Anfragen an das KVV verwendet werden. Cookies sind kleine Textdaten, die normalerweise vom Browser automatisiert, im Falle der App manuell auf dem Gerät gespeichert werden. Ein Cookie beinhaltet eine eindeutige, wiedererkennbare Zeichenfolge zur Identfizierung des Benutzers bei erneuten Anfragen.

Bei weiteren Anfragen (zum Beispiel Modulauflistung oder -details) werden diese mit Hilfe der Cookies und der IP-Adresse des Nutzers vom KVV-Server geladen und im privaten App-Speichers des Telefons gespeichert. Android unterscheidet in verschiedene Speichermöglichen. Der private Speicherbereich ist nur für die App zugänglich und kann nicht von anderen Anwendungen oder dem Telefonbetriebssystem ausgelesen werden.

#### Rechtsgrundlage für die Datenverarbeitung

Die Rechtsgrundlage für die Verwendung personenbezogener Daten unter der Verwendung von Cookies zur Identifizierung und der IP-Adresse des Nutzers ist hierfür der [Art. 6 der DSGVO](https://dejure.org/gesetze/DSGVO/6.html).

#### Widerrufsrecht

Um eine Speicherung der Cookies sowie der Moduldaten und die Verwendung der IP-Adresse zu widerrufen, kann der Nutzer sich ausloggen oder die App deinstallieren.

#### Dauer der Speicherung und Beseitigungsmöglichkeit

Bis zur Deinstallation oder Logout bleiben die zuletzt verwendeten Cookies sowie die aktuellste geladene Version der Moduldaten in dem privaten App-Speicher des Telefons gespeichert.

### OpenMensa

Von dem Dienst OpenMensa wird der aktuelle Mensa-Plan für die Mensen der FU Berlin geladen. Bei einem Aufruf der Schnittstelle wird nur die IP-Adresse des Nutzers an den Dienst gesendet.

Die Antwort wird ebenfalls in dem privaten Speicherbereich der App gespeichert und bis zur nächsten Aktualisierung vorgehalten.

#### Rechtsgrundlage für die Datenverarbeitung

Die Rechtsgrundlage für die Verwendung personenbezogener Daten unter der Verwendung von Cookies zur Identifizierung und der IP-Adresse des Nutzers ist hierfür der [Art. 6 der DSGVO](https://dejure.org/gesetze/DSGVO/6.html).

#### Widerrufsrecht

Um eine Speicherung der Daten zu den Mensen und die Verwendung der IP-Adresse zu widerrufen, kann der Nutzer die App deinstallieren.

#### Dauer der Speicherung und Beseitigungsmöglichkeit

Bis zur Deinstallation bleibt die aktuellste geladene Version der Daten zu den Mensen in dem privaten App-Speicher des Telefons gespeichert.